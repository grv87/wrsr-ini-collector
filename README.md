WRSR ini collector
==================

## Limitations

What this tool isn't able to do:

* It (currently) cannot make calculations based on the model, including (but maybe not limited to):
  * Capacity of residential building (token `$STORAGE_LIVING_AUTO`)
  * Cost of construction (tokens `$COST_WORK_BUILDING_NODE`, 
`$COST_WORK_BUILDING_KEYWORD` and `$COST_WORK_BUILDING_ALL`)
  
  It may be done if there is existing Python library to handle NMFs and algorithms are known.
  [3DVision Blender plugins](https://www.dropbox.com/sh/xuyrj0he24rgzz4/AACL2yws9pCVIHt4fvho2m7ga?dl=0) exist
  and are written in Python. But they don't support NMFs created from OBJs.
