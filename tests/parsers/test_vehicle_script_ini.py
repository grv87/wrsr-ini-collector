import json
import unittest
from wrsr_ini_collector.parsers import vehicle_script_ini_parser
from pathlib import Path
from pyparsing import pyparsing_test as ppt
from wrsr_ini_collector.testfixtures import FixedTestCase

class TestVehicleScriptIni(ppt.TestParseResultsAsserts, unittest.TestCase, FixedTestCase):
    def setUp(self) -> None:
        self.maxDiff = None
    def test_validate(self):
        vehicle_script_ini_parser.validate()
    def test_vehicle(self):
        result = vehicle_script_ini_parser.parse_file(Path("tests/parsers/testcases/vehicle.script.ini"), parse_all=True)
        with Path('tests/parsers/expected/vehicle.script.json').open('r', encoding='UTF-8') as f:
            expected = json.load(f)
        # TODO: https://github.com/pyparsing/pyparsing/issues/431
        self.assertParseResultsEquals(result, expected_list=[expected])
