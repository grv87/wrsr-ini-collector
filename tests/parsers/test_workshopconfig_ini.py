import unittest
from pyparsing import pyparsing_test as ppt
from pathlib import Path
from wrsr_ini_collector.parsers import workshopconfig_ini_parser
from wrsr_ini_collector.testfixtures import FixedTestCase

class TestWorkshopConfigIni(ppt.TestParseResultsAsserts, unittest.TestCase, FixedTestCase):
    def setUp(self) -> None:
        self.maxDiff = None
    def test_validate(self):
        workshopconfig_ini_parser.validate()
    def test_building(self):
        result = workshopconfig_ini_parser.parse_file(Path("tests/parsers/testcases/workshopconfig.building.ini"), parse_all=True)
        self.assertParseResultsEquals(result, expected_list=[{
            'gui_noborder': True,
            'item_desc': 'Welcome to Communism.\n'
                        'This gate starts a road paved with good intentions.\n',
            'item_id': 24165435764,
            'item_name': 'Gate to (Communist) Heaven',
            'item_type': 'WORKSHOP_ITEMTYPE_BUILDING',
            'object_buildings': [
                'Gate_to_Heaven',
                'Gate_to_Heaven_2'
            ],
            'owner_id': 23132135568357645,
            'tags': [12],
            'target_buildings_skins': [
                {
                    'path': '24165435764/Gate_to_Heaven',
                    'material': 'skin1',
                    'material_e': '--'
                },
                {
                    'path': '24165435764/Gate_to_Heaven',
                    'material': 'skin2',
                    'material_e': 'skin2_e'
                }
            ],
           'visibility': 2
        }])
    def test_vehicle(self):
        result = workshopconfig_ini_parser.parse_file(Path("tests/parsers/testcases/workshopconfig.vehicle.ini"), parse_all=True)
        self.assertParseResultsEquals(result, expected_list=[{
            'item_desc': 'A rowing boat.\n-By Basil Peace\n-Enjoy rowing!',
            'item_id': 1234567890,
            'item_name': 'Rowing boat',
            'item_type': 'WORKSHOP_ITEMTYPE_VEHICLE',
            'object_vehicles': [
                'rowing_boat'
            ],
            'owner_id': 23132135568357645,
            'tags': [16],
            'target_objects_skins': [
                {
                    'path': '1234567890/rowing_boat',
                    'material': 'red.mtl'
                },
                {
                    'path': '1234567890/rowing_boat',
                    'material': 'green.mtl'
                }
            ],
            'visibility': 0
        }])
