import unittest
from pyparsing import pyparsing_test as ppt
from wrsr_ini_collector.parsers.common import *
from wrsr_ini_collector.testfixtures import FixedTestCase

class TestCommon(ppt.TestParseResultsAsserts, unittest.TestCase, FixedTestCase):
    def test_INT(self):
        self.assertParseAndCheckList(INT, "10", [10])
        self.assertParseAndCheckList(INT, "-10", [-10])
        self.assertRaisesParseException(INT, "- 10")

    def test_keyword_name_value_group(self):
        self.assertParseAndCheckList(keyword_name_value_group("TEST"), "$TEST_BUILDING", [["test", "BUILDING"]])
        # TODO: How this passes without Combine?
        self.assertRaisesParseException(keyword_name_value_group("TEST"), "$TEST_ BUILDING")

    def test_keyword_name_value_group_in_dict(self):
        self.assertParseAndCheckDictFixed(dict_as_dict(keyword_name_value_group("TEST_ENUM")), "$TEST_ENUM_VALUE", {"test_enum": "VALUE"})

    def test_token_name_value_group(self):
        self.assertParseAndCheckList(token_name_value_group("TEST"), "TEST_BUILDING", [["test", "BUILDING"]])
        # TODO: How this passes without Combine?
        self.assertRaisesParseException(token_name_value_group("TEST"), "TEST_ BUILDING")

    def test_repeated_to_dict(self):
        self.assertParseAndCheckDictFixed(repeated_to_dict(["x", "y", "z"], FLOAT), "1.2 3.4 5.6", {"x": 1.2, "y": 3.4, "z": 5.6})

    def test_keyword_start(self):
        ast = keyword_start("TEST") + UPCASE_TOKEN
        self.assertParseAndCheckList(ast, "$TEST_BUILDING", ["TEST", "BUILDING"])

    def test_keyword_end(self):
        ast = pp.Literal("$TEST_") + keyword_end("BUILDING") + UPCASE_TOKEN
        self.assertParseAndCheckList(ast, "$TEST_BUILDING  BAR", ["$TEST_", "BUILDING", "BAR"])
        self.assertParseAndCheckList(ast, "$TEST_ BUILDING  BAR", ["$TEST_", "BUILDING", "BAR"])
        self.assertRaisesParseException(ast, "$TEST_BUILDING_BAR")

    def test_keyword_start_end(self):
        ast = pp.Combine(keyword_start("TEST") + keyword_end("BUILDING"))
        self.assertParseAndCheckList(ast, "$TEST_BUILDING", ["TESTBUILDING"])
        self.assertRaisesParseException(ast, "$TEST_ BUILDING")

    def test_combine_keyword_start_literal_end(self):
        ast = pp.Combine(keyword_start("TEST") + pp.Literal("FOO") + TOKEN_SEPARATOR + keyword_end("BUILDING"))
        self.assertParseAndCheckList(ast, "$TEST_FOO_BUILDING", ["TESTFOO_BUILDING"])
        self.assertRaisesParseException(ast, "$TEST_FOO_ BUILDING")
        self.assertRaisesParseException(ast, "$TEST_FOO _ BUILDING")
        self.assertRaisesParseException(ast, "$TEST_FOO _BUILDING")
        self.assertRaisesParseException(ast, "$TEST_ FOO_BUILDING")
        self.assertRaisesParseException(ast, "$TEST _ FOO_BUILDING")
        self.assertRaisesParseException(ast, "$TEST _FOO_BUILDING")

    def test_keyword(self):
        ast = UPCASE_TOKEN + keyword("TEST_BUILDING") + UPCASE_TOKEN
        self.assertParseAndCheckList(ast, "FOO $TEST_BUILDING BAR", ["FOO", "test_building", "BAR"])
        self.assertRaisesParseException(ast, "FOO$TEST_BUILDING BAR")
        self.assertRaisesParseException(ast, "FOO $TEST_BUILDING_BAR")

    def test_VEC2_DICT(self):
        self.assertParseAndCheckDictFixed(VEC2_DICT, "-10 5", {"x": -10.0, "z": 5.0}, verbose=False)

    def test_VEC3_DICT(self):
        self.assertParseAndCheckDictFixed(VEC3_DICT, "-10 0 5", {"x": -10.0, "y": 0.0, "z": 5.0}, verbose=False)
        self.assertParseAndCheckDictFixed(VEC3_DICT, "-10 5", {"x": -10.0, "z": 5.0}, verbose=False)

    def test_POINTS_VEC3_2_GROUPS(self):
        self.assertParseAndCheckDictFixed(dict_as_dict(POINTS_VEC3_2_GROUPS), "-10 0 5 10 0 -5", {
            "point1": {"x": -10.0, "y": 0.0, "z": 5.0},
            "point2": {"x": 10.0, "y": 0.0, "z": -5.0}
        }, verbose=False)

    def test_RESOURCE_TRANSPORT_GROUP(self):
        self.assertParseAndCheckList(RESOURCE_TRANSPORT_GROUP, "RESOURCE_TRANSPORT_COVERED", [["resource_transport", "COVERED"]])
        self.assertParseAndCheckList(RESOURCE_TRANSPORT_GROUP, "RESOURCE_TRANSPORT_NUCLEAR2", [["resource_transport", "NUCLEAR2"]])
