import json
import unittest
import pyparsing as pp
from pyparsing import pyparsing_test as ppt
from pathlib import Path
from wrsr_ini_collector.parsers import building_ini_parser
from wrsr_ini_collector.parsers.building_ini import create_cost_work_group_parser
from wrsr_ini_collector.parsers.common import dict_as_dict
from wrsr_ini_collector.testfixtures import FixedTestCase

class TestBuildingIni(ppt.TestParseResultsAsserts, unittest.TestCase, FixedTestCase):
    def setUp(self) -> None:
        self.maxDiff = None
    def test_validate(self):
        building_ini_parser.validate()
    def test_cost_work_vehicle_stations(self):
        result = create_cost_work_group_parser().parse_file(Path("tests/parsers/testcases/building.cost_work.vehicle_stations.ini"), parse_all=True)
        self.assertParseResultsEquals(result, expected_list=[[
            'SOVIET_CONSTRUCTION_ROOFTOP_BUILDING',
            {
                'factor': 0.8,
                'vehicle_stations': [
                    {
                        'point1': {'x': -10.0, 'y': 0.0, 'z': -5.0},
                        'point2': {'x': 10.0, 'y': 0.0, 'z': 5.0}
                    },
                    {
                        'point1': {'x': 5.0, 'y': 0.0, 'z': 10.0},
                        'point2': {'x': -5.0, 'y': 0.0, 'z': -10.0}
                    },
                ],
            }
        ]])

    def test_cost_work_cost_resource(self):
        result = create_cost_work_group_parser().parse_file(Path("tests/parsers/testcases/building.cost_work.cost_resource.ini"), parse_all=True)
        self.assertParseResultsEquals(result, expected_list=[[
            'SOVIET_CONSTRUCTION_ROOFTOP_BUILDING',
            {
                'factor': 0.8,
                'cost_resource': {
                    'steel': 50.0,
                    'bricks': 9.5,
                },
            }
        ]])

    def test_cost_work_cost_resource_auto(self):
        result = create_cost_work_group_parser().parse_file(Path("tests/parsers/testcases/building.cost_work.cost_resource_auto.ini"), parse_all=True)
        self.assertParseResultsEquals(result, expected_list=[[
            'SOVIET_CONSTRUCTION_ROOFTOP_BUILDING',
            {
                'factor': 0.8,
                'cost_resource_auto': {
                    'roof_woodbrick': 5.0,
                    'roof_woodsteel': 2.0,
                },
            }
        ]])

    def test_cost_work_vehicle_station_according_nodes(self):
        result = create_cost_work_group_parser().parse_file(Path("tests/parsers/testcases/building.cost_work.vehicle_station_according_nodes.ini"), parse_all=True)
        self.assertParseResultsEquals(result, expected_list=[[
            'SOVIET_CONSTRUCTION_ROOFTOP_BUILDING',
            {
                'factor': 0.8,
                'vehicle_station_according_nodes': [
                    'Main',
                    'Secondary',
                ],
            }
        ]])

    def test_cost_work_building_all(self):
        result = create_cost_work_group_parser().parse_file(Path("tests/parsers/testcases/building.cost_work.building_all.ini"), parse_all=True)
        self.assertParseResultsEquals(result, expected_list=[[
            'SOVIET_CONSTRUCTION_GROUNDWORKS',
            {
                'factor': 1.5,
                'building_all': True,
            }
        ]])

    def test_costs_works(self):
        result_dict = dict_as_dict(pp.ZeroOrMore(create_cost_work_group_parser())).parse_file(Path("tests/parsers/testcases/building.costs_works.ini"), parse_all=True)
        # TODO: https://github.com/pyparsing/pyparsing/issues/431
        self.assertParseResultsEquals(result_dict, expected_list=[{
            'SOVIET_CONSTRUCTION_GROUNDWORKS': {
                'factor': 1.5,
                'building_nodes': ['MainBuildingNode'],
                'building_keywords': ['$Tower'],
            },
            'SOVIET_CONSTRUCTION_ROOFTOP_BUILDING': {
                'factor': 0.8,
                'cost_resource_auto': {
                    'roof_woodbrick': 5.0,
                },
                'cost_resource': {
                    'steel': 50.0,
                },
                'vehicle_station_according_nodes': ['MainVehicleStationAccordingNode'],
                'vehicle_stations': [
                    {
                        'point1': {'x': -10.0, 'y': 0.0, 'z': -5.0},
                        'point2': {'x': 10.0, 'y': 0.0,'z': 5.0}
                    }
                ],
                'building_all': True,
                'building_nodes': ['MainBuildingNode2'],
            }
        }])

    def test_minimum(self):
        result = building_ini_parser.parse_file(Path("tests/parsers/testcases/building.minimum.ini"), parse_all=True)
        # TODO: https://github.com/pyparsing/pyparsing/issues/431
        self.assertParseResultsEquals(result, expected_list=[{
            'name_str': 'Test building',
            'type': 'UNIVERSITY',
        }])

    def test_comments_1(self):
        result = building_ini_parser.parse_file(Path("tests/parsers/testcases/building.comments.1.ini"), parse_all=True)
        # TODO: https://github.com/pyparsing/pyparsing/issues/431
        self.assertParseResultsEquals(result, expected_list=[{
            'name_str': 'Test building',
            'type': 'UNIVERSITY',
        }])

    def test_comments_2(self):
        result = building_ini_parser.parse_file(Path("tests/parsers/testcases/building.comments.2.ini"), parse_all=True)
        # TODO: https://github.com/pyparsing/pyparsing/issues/431
        self.assertParseResultsEquals(result, expected_list=[{
            'name_str': 'Test building',
            'type': 'UNIVERSITY',
        }])

    def test_complete(self):
        result = building_ini_parser.parse_file(Path("tests/parsers/testcases/building.complete.ini"), parse_all=True)
        with Path('tests/parsers/expected/building.complete.json').open('r', encoding='UTF-8') as f:
            expected = json.load(f)
        # TODO: https://github.com/pyparsing/pyparsing/issues/431
        self.assertParseResultsEquals(result, expected_list=[expected])
