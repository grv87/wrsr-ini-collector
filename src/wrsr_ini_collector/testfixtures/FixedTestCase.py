class FixedTestCase:
    # Temporary fix for https://github.com/pyparsing/pyparsing/issues/431
    def assertParseAndCheckDictFixed(self, expr, test_string, expected_dict, msg=None, verbose=True):
        self.assertParseAndCheckList(expr, test_string, [expected_dict], msg, verbose)
