import json
from pathlib import Path
import logging
import argparse
import traceback
from functools import reduce
import operator
from .parsers import *
import winreg
import vdf

__version__ = "0.1.0"

class WrsrIniCollectorCli():
    game_id = "784150"

    def __init__(self, log_stacktrace = False):
        self.errors = 0
        self.log_stacktrace = log_stacktrace

    def try_parse(self, parser, fn):
        try:
            result = parser.parse_file(fn, parse_all=True, encoding='utf_8_sig')
        except Exception as e:
            self.errors += 1
            logging.error(f"Error #{self.errors} processing {fn}: {e}")
            if self.log_stacktrace:
                logging.exception(e)
            result = {"error": traceback.format_exception(e)}
            # TODO
            # if self.errors >= 100:
            #    raise e
        else:
            logging.info(f"Processed {fn}")
            assert len(result) == 1
            result = result[0]
            assert isinstance(result, dict)
        return result

    def collect_building_inis(self, dir: Path):
        return {
            file.stem: self.try_parse(building_ini_parser, file)
            for file in filter(lambda file: file.suffix == ".ini", dir.iterdir())
        }

    def collect_vehicle_script_inis(self, dir: Path):
        return {
            f"{dir.name}/{subdir.name}": self.try_parse(vehicle_script_ini_parser, subdir / "script.ini")
            for subdir in filter(lambda subdir: subdir.is_dir(), dir.iterdir())
        }

    def collect_workshopconfigs(self, dir: Path):
        mods = {}
        buildings = {}
        vehicles = {}
        for subdir in filter(lambda subdir: subdir.is_dir(), dir.iterdir()):
            mod_id = subdir.name
            mod = self.try_parse(workshopconfig_ini_parser, subdir / "workshopconfig.ini")
            for building_path in mod.pop("object_buildings", []):
                buildings[f"{mod_id}/{building_path}"] = self.try_parse(building_ini_parser, subdir / building_path / "building.ini")
            for vehicle_path in mod.pop("object_vehicles", []):
                vehicles[f"{mod_id}/{vehicle_path}"] = self.try_parse(vehicle_script_ini_parser, subdir / vehicle_path / "script.ini")
            mods[mod_id] = mod
        return (mods, buildings, vehicles)

    def collect_all(self, steam_dir: Path):
        media_soviet_dir = steam_dir / "steamapps/common/SovietRepublic/media_soviet"

        buildings = self.collect_building_inis(media_soviet_dir / "buildings_types")

        vehicles = reduce(operator.__or__, map(
            lambda subdir: self.collect_vehicle_script_inis(media_soviet_dir / subdir),
            ["airplanes", "cabins", "containers", "helicopters", "ships", "trains", "vehicles"]
        ))

        mods = {}
        for dir in [
            steam_dir / "steamapps/workshop/content" / WrsrIniCollectorCli.game_id,
            media_soviet_dir / "workshop_wip",
        ]:
            m, b, v = self.collect_workshopconfigs(dir)
            mods |= m
            buildings |= b
            vehicles |= v

        return {"buildings": buildings, "vehicles": vehicles, "other_mods": mods}

    def collect_folder(self, dir: Path):
        result = {
            "buildings": {},
            "vehicles": {}
        }
        for file in dir.rglob("*.ini"):
            parent = file.parent
            path = parent.relative_to(dir).parts if parent != dir else []
            try:
                parser_name = WrsrIniCollectorCli.get_parser_name_by_file_name(file.name)
            except KeyError:
                parser_name = "building"
                path += [file.stem]
            result[parser_name + "s"]["/".join(path)] = self.try_parse(WrsrIniCollectorCli.parsers[parser_name], file)
        return result

    def save_result_file(self, output_file: Path, result):
        with open(output_file, "w") as f:
            json.dump(result, f, indent=4)

    def get_game_library():
        steam_path = None
        with winreg.OpenKey(winreg.HKEY_CURRENT_USER, "Software\\Valve\\Steam") as key:
            steam_path = Path(winreg.QueryValueEx(key, "SteamPath")[0])
        with open(steam_path / "config/libraryfolders.vdf", 'r') as f:
            libraries = vdf.load(f)
            for id, library in libraries["libraryfolders"].items():
                if WrsrIniCollectorCli.game_id in library.get("apps", {}):
                    return Path(library["path"])
        return None

    def all(self, output_dir: Path):
        game_library_dir = WrsrIniCollectorCli.get_game_library()
        logging.info(f"Detected game library dir: {game_library_dir}")
        output_dir.mkdir(parents=True, exist_ok=True)
        for name, result in self.collect_all(game_library_dir).items():
            self.save_result_file(output_dir / f"{name}.json", result)

    parsers = {
        "building": building_ini_parser,
        "vehicle": vehicle_script_ini_parser,
        "workshopconfig": workshopconfig_ini_parser,
    }
    def get_parser_name_by_file_name(file_name):
        fns = {
            "building.ini": "building",
            "script.ini": "vehicle",
            "workshopconfig.ini": "workshopconfig",
        }
        return fns[file_name]

    def folder(self, folder: Path, output_dir: Path):
        output_dir.mkdir(parents=True, exist_ok=True)
        for name, result in self.collect_folder(folder).items():
            self.save_result_file(output_dir / f"{name}.json", result)

    def single(self, single_file: Path, output_file: Path, type = None):
        parser = None
        if type:
            parser = WrsrIniCollectorCli.parsers[type]
        else:
            try:
                parser = WrsrIniCollectorCli.parsers[WrsrIniCollectorCli.get_parser_name_by_file_name(output_file.name)]
            except KeyError as e:
                logging.error(f"{single_file.name} has an unknown file name. Use --type to specify how this file should be parsed")
                # TOTHINK
                # argparser.print_usage()
                raise e
        result = self.try_parse(parser, single_file)
        output_file.parent.mkdir(parents=True, exist_ok=True)
        self.save_result_file(output_file, result)

def main():
    cli = WrsrIniCollectorCli()
    argparser = argparse.ArgumentParser(description = "Collect data from WRSR INIs (built-in and mods).")

    subparsers = argparser.add_subparsers()

    all_command = subparsers.add_parser("all")
    all_command.add_argument('-o', '--output-dir', type=Path, default=".")
    all_command.set_defaults(func=cli.all)

    folder_command = subparsers.add_parser("folder")
    folder_command.add_argument('folder', type=Path)
    folder_command.add_argument('-o', '--output-dir', type=Path, default=".")
    folder_command.set_defaults(func=cli.folder)

    single_command = subparsers.add_parser("single")
    single_command.add_argument('single_file', type=Path)
    single_command.add_argument('--type', choices=WrsrIniCollectorCli.parsers.keys())
    single_command.add_argument('-o', '--output-file', type=Path)
    single_command.set_defaults(func=cli.single)

    argparser.add_argument('--stacktrace', action=argparse.BooleanOptionalAction)

    args = vars(argparser.parse_args())
    cli.log_stacktrace = args.pop("stacktrace")
    args.pop("func")(**args)
