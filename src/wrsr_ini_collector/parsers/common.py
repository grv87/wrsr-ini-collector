from turtle import down
import pyparsing as pp
pp.ParserElement.enable_packrat()

SIGN = pp.Opt(pp.Char("-"))
NUMS = pp.Word(pp.nums)
FRACTIONAL_PART = pp.Opt("." + NUMS)

UNSIGNED_INT = NUMS.copy().add_parse_action(pp.common.convert_to_integer)
INT = pp.Combine(SIGN + NUMS).add_parse_action(pp.common.convert_to_integer)
UNSIGNED_FLOAT = pp.Combine(NUMS + FRACTIONAL_PART).add_parse_action(pp.common.convert_to_float)
FLOAT = pp.Combine(SIGN + NUMS + FRACTIONAL_PART).add_parse_action(pp.common.convert_to_float)
# TOTHINK
# UNSIGNED_INT = pp.pyparsing_common.integer
# INT = pp.pyparsing_common.signed_integer
# UNSIGNED_FLOAT = pp.pyparsing_common.real # REMOVEME ?
# FLOAT = pp.pyparsing_common.real

STRING = pp.QuotedString(quote_char = '"', multiline = True, end_quote_char = '"\r\n')

UPCASE_TOKEN_INIT_CHARS = pp.srange("[A-Z_]")
UPCASE_TOKEN_CHARS = pp.srange("[A-Z0-9_]")
TOKEN_CHAR = "_"
TOKEN_SEPARATOR = pp.Char(TOKEN_CHAR)
SUPPRESSED_TOKEN_SEPARATOR = TOKEN_SEPARATOR.suppress()
UPCASE_TOKEN = pp.Word(init_chars=UPCASE_TOKEN_INIT_CHARS, body_chars=UPCASE_TOKEN_CHARS)

DOLLAR = pp.Char("$")
SUPPRESSED_DOLLAR = DOLLAR.suppress()

def keyword_start(start):
    return SUPPRESSED_DOLLAR + pp.Literal(start) + SUPPRESSED_TOKEN_SEPARATOR

def keyword_end(end):
    # https://github.com/pyparsing/pyparsing/issues/433
    # return pp.Keyword(end, ident_chars=TOKEN_CHARS)
    # https://github.com/pyparsing/pyparsing/issues/434
    # return pp.Literal(end) + pp.NotAny(pp.Char(TOKEN_CHARS))
    return pp.Literal(end) + pp.NotAny(pp.Char(UPCASE_TOKEN_CHARS).leave_whitespace())

def keyword(s, downcase = True):
    # https://github.com/pyparsing/pyparsing/issues/433
    keyword = keyword_end(s)
    if downcase:
        keyword = keyword.add_parse_action(pp.common.downcase_tokens)
    return pp.Combine(SUPPRESSED_DOLLAR + keyword)
    # return pp.Combine(SUPPRESSED_DOLLAR + pp.Keyword(s, ident_chars=TOKEN_CHARS).add_parse_action(pp.common.downcase_tokens))

true_lambda = lambda: True
# def parse_flag(t):
#     return [[t[0], True]] if len(t) > 0 else t

def flag_group(element_or_s):
    if isinstance(element_or_s, str):
        element_or_s = keyword(element_or_s)
    # Is it valid/supported to repeat flags?
    return pp.Opt(pp.Group(element_or_s.add_parse_action(pp.common.downcase_tokens) + pp.Empty().add_parse_action(true_lambda)))
    # TODO: Doesn't work
    # return pp.ZeroOrMore(pp.Group(element_or_s.add_parse_action(pp.common.downcase_tokens) + pp.Empty().add_parse_action(true_lambda)))
    # return pp.ZeroOrMore(element_or_s.add_parse_action(pp.common.downcase_tokens)).add_parse_action(lambda t: parse_flag(t))

# TODO
def flag_group_not_opt(element_or_s):
    if isinstance(element_or_s, str):
        element_or_s = keyword(element_or_s)
    return pp.Group(element_or_s.add_parse_action(pp.common.downcase_tokens) + pp.Empty().add_parse_action(true_lambda))

def flag_with_results_name(element_or_s, results_name = None):
    if isinstance(element_or_s, str) and results_name == None:
        results_name = element_or_s.lower()
        element_or_s = keyword(element_or_s)
    return pp.Opt(element_or_s.suppress() + pp.Empty().add_parse_action(true_lambda).set_results_name(results_name))

def keyword_name_value_group(start):
    # TOTEST: word start/end/spaces
    return pp.Group(
        keyword_start(start).add_parse_action(pp.common.downcase_tokens) +
        UPCASE_TOKEN
    )

# TODO: digits in all other places ?
def token_name_value_group(start):
    return pp.Group(
        pp.Literal(start).add_parse_action(pp.common.downcase_tokens) +
        SUPPRESSED_TOKEN_SEPARATOR +
        UPCASE_TOKEN
    )

def get_name(name):
    return name

def named_group(name, element):
    return pp.Group(pp.Empty().add_parse_action(lambda: get_name(name), call_during_try = True) + element)

def repeated_to_groups(fields, element):
    return pp.And([named_group(name, element) for name in fields])

def repeated_to_dict(fields, element):
    return dict_as_dict(repeated_to_groups(fields, element))


def each_for_dict(unique_values = [], lists_of_singles = {}, lists_of_structures = {}, dicts = {}, unknown = False):
    def get_fix_each_for_dict(nested_dicts = []):
        def fix_each_for_dict(pr):
            # as_dict here doesn't run on Dict.
            # So, https://github.com/pyparsing/pyparsing/issues/431 doesn't apply here
            # It works on names set with set_results_name
            pr_as_dict = pr.as_dict()
            for key in nested_dicts:
                if key in pr_as_dict.keys():
                    pr_as_dict[key] = dict(pr_as_dict[key])
            return list(pr_as_dict.items())
        return lambda pr: fix_each_for_dict(pr)
    def to_dict(pr):
        if len(pr) == 2:
            pr[pr[0]] = pr[1]
        return pr
    def ungroup(pr):
        return pr.as_list()[0] if len(pr) == 1 else pr
    result = pp.Each(
        # TODO: remove ungroup
        [element.add_parse_action(ungroup).add_parse_action(to_dict) for element in unique_values] +
        [pp.ZeroOrMore(element.add_parse_action(ungroup).set_results_name(key, list_all_matches=True)) for key, element in lists_of_singles.items()] +
        # TODO: for dicts replacing dict_to_dict with Dict removes necessity to ungroup
        [pp.ZeroOrMore(element.add_parse_action(ungroup).set_results_name(key, list_all_matches=True)) for key, element in lists_of_structures.items()] +
        [pp.ZeroOrMore(element.set_results_name(key, list_all_matches=True)) for key, element in dicts.items()] +
        # TODO
        ([pp.ZeroOrMore(UNKNOWN_WORD.set_results_name("invalid_unknown", list_all_matches=True))] if unknown else [])
    ).add_parse_action(get_fix_each_for_dict(dicts.keys()), call_during_try = True)
    return result

def dict_as_dict(elements):
    return pp.Dict(elements, asdict=True)

VEC2_DICT = repeated_to_dict(["x", "z"], FLOAT)
# VEC3_DICT = repeated_to_dict(["x", "y", "z"], FLOAT)
# TOTEST: Is it valid/supported?
VEC3_DICT = FLOAT[2, 3].set_parse_action(lambda t: {"x": t[0], "z": t[1]} if len(t) == 2 else {"x": t[0], "y": t[1], "z": t[2]})
POINT_VEC3_GROUP = named_group("point", VEC3_DICT)
POINTS_VEC2_2_GROUPS = repeated_to_groups(["point1", "point2"], VEC2_DICT)
POINTS_VEC3_2_GROUPS = repeated_to_groups(["point1", "point2"], VEC3_DICT)

RESOURCE_TRANSPORT_GROUP = token_name_value_group("RESOURCE_TRANSPORT")
PARTICLE_EFFECT_NAME = pp.Word(pp.srange("[a-z0-9_]"))

PATH = pp.Word(pp.srange("[a-zA-Z0-9_/\\\\.()'&[\\]$+-]"))

def str_or_predefined_by_int_group(s):
    return pp.Group(pp.MatchFirst([
        keyword(f"{s}_STR") + STRING,
        # Is it valid?
        keyword(s).set_parse_action(lambda: f"{s.lower()}_str") + STRING,
        keyword(s) + UNSIGNED_INT,
    ]))

RESOURCE = pp.one_of([
    "workers",
    "eletric",
    "vehicles",
    "trains",
    "heat",
    "gravel",
    "rawgravel",
    "plants",
    "steel",
    "aluminium",
    "prefabpanels",
    "bricks",
    "wood",
    "oil",
    "chemicals",
    "coal",
    "rawcoal",
    "iron",
    "rawiron",
    "bauxite",
    "rawbauxite",
    "bitumen",
    "boards",
    "uranium",
    "yellowcake",
    "uf6",
    "nuclearfuel",
    "nuclearfuelburned",
    "fuel",
    "fabric",
    "alcohol",
    "cement",
    "alumina",
    "food",
    "clothes",
    "meat",
    "livestock",
    "asphalt",
    "concrete",
    "ecomponents",
    "mcomponents",
    "plastics",
    "eletronics",
    "water",
    "usagewater",
], as_keyword=True)

# TODO: support other character sets as well
MODEL_PART = pp.Word(pp.pyparsing_unicode.alphanums + "_:.?=&+-")

unknown_chars = pp.pyparsing_unicode.alphanums + "_:/\\\\.()'&?[\\]№%=+-"
# UNKNOWN_WORD = pp.Word(pp.printables)
UNKNOWN_WORD = pp.NotAny(pp.Keyword("end", caseless=True)) + pp.Word(unknown_chars, unknown_chars + "$")
