from .common import *

def create_cost_work_group_parser():
    PHASE = pp.one_of([
        "SOVIET_CONSTRUCTION_GROUNDWORKS",
        "SOVIET_CONSTRUCTION_BOARDS_LAYING",
        "SOVIET_CONSTRUCTION_BRICKS_LAYING",
        "SOVIET_CONSTRUCTION_SKELETON_CASTING",
        "SOVIET_CONSTRUCTION_STEEL_LAYING",
        "SOVIET_CONSTRUCTION_PANELS_LAYING",
        "SOVIET_CONSTRUCTION_ROOFTOP_BUILDING",
        "SOVIET_CONSTRUCTION_WIRE_LAYING",
        "SOVIET_CONSTRUCTION_TUNNELING",
        "SOVIET_CONSTRUCTION_INTERIOR_WORKS",
        "SOVIET_CONSTRUCTION_BRIDGE_BUILDING",
        "SOVIET_CONSTRUCTION_RAILWAY_LAYING",
        "SOVIET_CONSTRUCTION_ASPHALT_ROLLING",
        "SOVIET_CONSTRUCTION_ASPHALT_LAYING",
        "SOVIET_CONSTRUCTION_GRAVEL_LAYING",
    ], as_keyword=True)
    PREDEFINED_COST = pp.one_of([
        "ground_asphalt",
        "ground",
        "steel",
        "wall_concrete",
        "wall_panels",
        "wall_brick",
        "wall_steel",
        "wall_wood",
        "tech_steel",
        "electro_steel",
        "techelectro_steel",
        "roof_woodbrick",
        "roof_steel",
        "roof_woodsteel",
        "roof_asphalt",
    ], as_keyword=True)

    cost_work_building_node = keyword("COST_WORK_BUILDING_NODE").suppress() + MODEL_PART
    # TOTEST: Is it valid to suppress DOLLAR ?
    cost_work_building_keyword = keyword("COST_WORK_BUILDING_KEYWORD").suppress() + pp.Combine(pp.Opt(DOLLAR) + MODEL_PART)
    cost_work_vehicle_station_according_node = keyword("COST_WORK_VEHICLE_STATION_ACCORDING_NODE").suppress() + MODEL_PART

    cost_resource_auto_group = pp.Group(keyword("COST_RESOURCE_AUTO").suppress() + PREDEFINED_COST + UNSIGNED_FLOAT)
    cost_resource_group = pp.Group(keyword("COST_RESOURCE").suppress() + RESOURCE + UNSIGNED_FLOAT) # POSITIVE_INT ?

    cost_work_vehicle_station_dict = dict_as_dict(keyword("COST_WORK_VEHICLE_STATION").suppress() + POINTS_VEC3_2_GROUPS)

    building_all_flag_group = flag_group(pp.Combine(
        keyword_start("COST_WORK").suppress() +
        keyword_end("BUILDING_ALL")
    ))

    return pp.Group(
        keyword("COST_WORK").suppress() +
        PHASE +
        dict_as_dict(
            named_group("factor", UNSIGNED_FLOAT) +
            each_for_dict(
                unique_values = [
                    building_all_flag_group,
                ],
                lists_of_singles = {
                    "building_nodes": cost_work_building_node,
                    "building_keywords": cost_work_building_keyword,
                    "vehicle_station_according_nodes": cost_work_vehicle_station_according_node,
                },
                lists_of_structures = {
                    "vehicle_stations": cost_work_vehicle_station_dict,
                },
                dicts = {
                    "cost_resource_auto": cost_resource_auto_group,
                    "cost_resource": cost_resource_group,
                },
                unknown = True
            )
        )
    )

def create_building_ini_parser():
    name_group = str_or_predefined_by_int_group("NAME")

    type_group = keyword_name_value_group("TYPE")
    subtype_group = keyword_name_value_group("SUBTYPE")

    # TODO: SET
    resource_source_flag_groups = [
        flag_group("RESOURCE_SOURCE_WORKERS"),
        flag_group("RESOURCE_SOURCE_GRAVEL"),
        flag_group("RESOURCE_SOURCE_OPEN"),
        flag_group("RESOURCE_SOURCE_OPEN_BOARDS"),
        flag_group("RESOURCE_SOURCE_OPEN_BRICKS"),
        flag_group("RESOURCE_SOURCE_OPEN_PANELS"),
        flag_group("RESOURCE_SOURCE_ASPHALT"),
        flag_group("RESOURCE_SOURCE_CONCRETE"),
        flag_group("RESOURCE_SOURCE_COVERED"),
        flag_group("RESOURCE_SOURCE_COVERED_ELECTRO"),
        flag_group("RESOURCE_SOURCE_WATER"),
        flag_group("RESOURCE_SOURCE_SEWAGE"),
    ]

    factor_groups = [pp.Opt(pp.Group(keyword(s) + UNSIGNED_FLOAT)) for s in [
        "ELETRIC_CONSUMPTION_LIVING_WORKER_FACTOR_ABLE_SERVE",
        "ELETRIC_CONSUMPTION_LIVING_WORKER_FACTOR",
        "ELETRIC_WITHOUT_WORKING_FACTOR",
        "ELETRIC_CONSUMPTION_LIGHTING_WORKER_FACTOR_ABLE_SERVE",
        "ELETRIC_CONSUMPTION_LIGHTING_WORKER_FACTOR",
        "ELETRIC_WITHOUT_LIGHTING_FACTOR",
        "WATER_CONSUMPTION_LIVING_WORKER_FACTOR_ABLE_SERVE",
        "WATER_CONSUMPTION_LIVING_WORKER_FACTOR",
        "WATER_WITHOUT_WORKING_FACTOR",
        "VEHICLE_LOADING_FACTOR",
        "VEHICLE_UNLOADING_FACTOR",
        "ELETRIC_CONSUMPTION_HEATING_WORKER_FACTOR",
        "HEATING_WITHOUT_WORKING_FACTOR",
    ]] + [pp.Opt(pp.Group(keyword(s) + FLOAT)) for s in [
        "ELETRIC_CONSUMPTION_LOADING_FIXED",
        "ELETRIC_CONSUMPTION_UNLOADING_FIXED",
    ]]

    production_group = pp.Group(keyword("PRODUCTION").suppress() + RESOURCE + UNSIGNED_FLOAT)
    consumption_group = pp.Group(keyword("CONSUMPTION").suppress() + RESOURCE + UNSIGNED_FLOAT)
    consumption_per_second_group = pp.Group(keyword("CONSUMPTION_PER_SECOND").suppress() + RESOURCE + UNSIGNED_FLOAT)

    workers_needed_group = pp.Group(keyword("WORKERS_NEEDED") + UNSIGNED_INT)
    profesors_needed_group = pp.Group(keyword("PROFESORS_NEEDED") + UNSIGNED_INT)
    citizen_able_serve_group = pp.Group(keyword("CITIZEN_ABLE_SERVE") + UNSIGNED_INT)
    working_vehicles_needed_group = pp.Group(keyword("WORKING_VEHICLES_NEEDED") + UNSIGNED_INT)

    # NB: MatchFirst most probably won't work, since prefixes IMPORT and EXPORT are ambiguous
    # Didn't test it though
    storage_dict = dict_as_dict(
        pp.Or([
            named_group("type", pp.MatchFirst([
                pp.Combine(
                    keyword_start("STORAGE").suppress() +
                    pp.MatchFirst([
                        keyword_end("FUEL"),
                        keyword_end("EXPORT"),
                        keyword_end("IMPORT_CARPLANT"),
                        keyword_end("IMPORT"),
                        keyword_end("DEMAND_BASIC"),
                        keyword_end("DEMAND_MEDIUM"),
                        keyword_end("DEMAND_MEDIUMADVANCED"),
                        keyword_end("DEMAND_ADVANCED"),
                        keyword_end("DEMAND_HOTEL"),
                        keyword_end("DEMAND_PRISON"),
                    ])
                ),
                keyword("STORAGE").suppress() + pp.Empty().add_parse_action(lambda: ""),
            ])) +
            RESOURCE_TRANSPORT_GROUP +
            named_group("amount", UNSIGNED_FLOAT),

            named_group("type", pp.Combine(
                keyword_start("STORAGE").suppress() +
                pp.MatchFirst([
                    keyword_end("IMPORT_SPECIAL"),
                    keyword_end("EXPORT_SPECIAL"),
                    keyword_end("SPECIAL"),
                ])
            )) +
            RESOURCE_TRANSPORT_GROUP +
            named_group("amount", UNSIGNED_FLOAT) +
            named_group("resource", RESOURCE),
            named_group("type", pp.Combine(
                keyword_start("STORAGE").suppress() +
                keyword_end("LIVING_AUTO")
            )) +
            named_group("model_part", MODEL_PART),

            named_group("type", pp.Combine(
                keyword_start("STORAGE").suppress() +
                pp.MatchFirst([
                    keyword_end("UNPACKCONTAINERS_TO_STORAGE"),
                    keyword_end("PACKCONTAINERS_FROM_STORAGE"),
                ])
            )) +
            named_group("amount", UNSIGNED_INT),
        ])
    )

    # TODO: connect with storage by #
    resource_visualization_dict = dict_as_dict(
        keyword("RESOURCE_VISUALIZATION").suppress() +
        # Not unique. One storage may have several visualizations
        named_group("storage_index", UNSIGNED_INT) +
        # TOTEST: Is it valid/supported?
        # TODO: fix in output
        pp.Group(pp.MatchFirst([pp.Keyword("position"), pp.Keyword("positon")]) + VEC3_DICT) +
        pp.Group(pp.Keyword("rotation") + FLOAT) +
        pp.Group(pp.Keyword("scale") + VEC3_DICT) +
        pp.Group(pp.Keyword("numstepx") + dict_as_dict(
            named_group("offsetX", FLOAT) +
            named_group("amount", UNSIGNED_FLOAT)
        )) +
        pp.Group(pp.Keyword("numstept") + dict_as_dict(
            named_group("offsetZ", FLOAT) +
            named_group("amount", UNSIGNED_FLOAT)
        ))
    )

    particle_dict = dict_as_dict(pp.MatchFirst([
        keyword("PARTICLE").suppress() +
        named_group("effect_name", PARTICLE_EFFECT_NAME) +
        POINT_VEC3_GROUP +
        # TOTEST: Are Opts valid?
        pp.Opt(
            named_group("alpha", UNSIGNED_FLOAT) +
            pp.Opt(named_group("scale", UNSIGNED_FLOAT))
        ),
        # TODO: remove prefix
        named_group("type", keyword("PARTICLE_SNOWREMOVE")) +
        POINT_VEC3_GROUP +
        named_group("alpha", UNSIGNED_FLOAT) +
        named_group("scale", UNSIGNED_FLOAT),

        # TODO: remove prefix
        named_group("type", keyword("PARTICLE_REACTOR")) +
        POINT_VEC3_GROUP,
    ]))
    pollution = keyword_start("POLLUTION").suppress() + UPCASE_TOKEN

    def dead_connection(start, end):
        return \
            named_group("type", pp.Combine(
                keyword_start(start).suppress() +
                keyword_end(end)
            )) + \
            named_group("point1", VEC3_DICT) + \
            keyword(start + TOKEN_CHAR + end).suppress() + \
            named_group("point2", VEC3_DICT)

    # TODO: test for spaces
    connection_dict = dict_as_dict(
        # TOTEST: Does MatchFirst works with named_group?
        pp.MatchFirst([
            named_group("type", pp.MatchFirst([
                pp.Combine(
                    keyword_start("CONNECTION").suppress() +
                    pp.MatchFirst([
                        keyword_end("ROAD_ALLOWPASS"),
                        keyword_end("ROAD_BORDER"),
                        keyword_end("ROAD_INPUT"),
                        keyword_end("ROAD_OUTPUT"),
                        keyword_end("ROAD_HEIGHT"),
                        keyword_end("ROAD"),
                        # TODO: Note: all allow pass connection must be first connections you define ?
                        keyword_end("RAIL_ALLOWPASS_INPUT"),
                        keyword_end("RAIL_ALLOWPASS_OUTPUT"),
                        keyword_end("RAIL_ALLOWPASS"),
                        keyword_end("RAIL_BORDER"),
                        keyword_end("RAIL_HEIGHT"),
                        keyword_end("RAIL"),
                        keyword_end("PEDESTRIAN_NOTPICK"),
                        keyword_end("PEDESTRIAN"),
                        keyword_end("CONNECTION"),
                        keyword_end("PIPE_INPUT"),
                        keyword_end("PIPE_OUTPUT"),
                        keyword_end("STEAM_INPUT"),
                        keyword_end("STEAM_OUTPUT"),
                        keyword_end("HEATING_SMALL"),
                        keyword_end("HEATING_BIG"),
                        keyword_end("CONVEYOR_INPUT"),
                        keyword_end("CONVEYOR_OUTPUT"),
                        keyword_end("BULK_INPUT"),
                        keyword_end("BULK_OUTPUT"),
                        keyword_end("ELETRIC_HIGH_INPUT"),
                        keyword_end("ELETRIC_HIGH_OUTPUT"),
                        keyword_end("ELETRIC_LOW_INPUT"),
                        keyword_end("ELETRIC_LOW_OUTPUT"),
                        keyword_end("CABLEWAY"),
                        keyword_end("AIRROAD"),
                        keyword_end("WATERPIPE_INPUT"),
                        keyword_end("WATERPIPE_OUTPUT"),
                        keyword_end("SEWAGE_INPUT"),
                        keyword_end("SEWAGE_OUTPUT"),
                        keyword_end("FENCE"),
                        keyword_end("TRAMROAD_DEAD"),
                        keyword_end("TRAMTROLLEYS_DEAD"),
                    ])
                ),
                pp.Combine(
                    keyword_start("SOVIET_BUILDING") +
                    pp.Literal("CONNECTION").suppress() +
                    keyword_end("_TRAM_SPECIAL")
                ),
            ])) +
            POINTS_VEC3_2_GROUPS,

            named_group("type", pp.Combine(
                keyword_start("CONNECTIONS").suppress() +
                pp.MatchFirst([
                    keyword_end("ROAD_DEAD_SQUARE"),
                    keyword_end("AIRPORT_DEAD_SQUARE"),
                    keyword_end("PEDESTRIAN_DEAD_SQUARE"),
                    keyword_end("SPACE"),
                ])
            )) +
            POINTS_VEC2_2_GROUPS,

            named_group("type", pp.Combine(
                keyword_start("CONNECTIONS").suppress() +
                keyword_end("WATER_DEAD_SQUARE")
            )) +
            named_group("height", UNSIGNED_FLOAT) +
            POINTS_VEC2_2_GROUPS,
        ] +
        [dead_connection(start, end) for start, end in [
            ("CONNECTION", "ROAD_DEAD"),
            ("CONNECTION", "MUDROAD_DEAD"),
            ("CONNECTION", "RAIL_DEAD"),
            ("CONNECTION", "PEDESTRIAN_DEAD"),
            ("CONNECTION", "AIRPORT_DEAD"),
            ("CONNECTION", "WATER_DEAD"),
            ("CONNECTIONS", "ROAD_DEAD"),
        ]]
        )
    )
    # TODO:
    # CONNECTION_AIRPLANE

    station_dict = dict_as_dict(
        named_group("type", pp.MatchFirst([
            pp.Combine(
                pp.MatchFirst([
                    keyword_start("VEHICLE"),
                    keyword_start("SHIP"),
                    keyword_start("HELIPORT"),
                ]) +
                keyword_end("STATION").suppress()
            ),
            pp.Combine(
                keyword_start("AIRPLANE") +
                pp.Literal("STATION").suppress() +
                SUPPRESSED_TOKEN_SEPARATOR +
                pp.MatchFirst([
                    keyword_end("30M"),
                    keyword_end("40M"),
                    keyword_end("50M"),
                    keyword_end("75M"),
                ])
            ),
            keyword("FIRESTATION"),
        ])) +
        POINTS_VEC3_2_GROUPS
    )
    parking_dict = dict_as_dict(
        named_group("type", pp.Combine(
            keyword_start("VEHICLE") +
            # MatchFirst не удалось использовать
            # При любом порядке вариантов были ошибки парсинга, то на одних файлах, то на других
            pp.Or([
                keyword_end("PARKING").suppress(),
                pp.Literal("PARKING").suppress() + TOKEN_SEPARATOR + keyword_end("PERSONAL"),
            ])
        )) +
        POINTS_VEC3_2_GROUPS
    )

    roadvehicle_notflip_flag_group = flag_group("ROADVEHICLE_NOTFLIP")
    roadvehicle_eletric_flag_group = flag_group("ROADVEHICLE_ELETRIC")
    roadvehicle_forklift_pass_flag_group = flag_group("ROADVEHICLE_FORKLIFT_PASS")
    roadvehicle_tram_flag_group = flag_group("ROADVEHICLE_TRAM")
    long_trains_flag_group = flag_group("LONG_TRAINS")
    station_not_block_flag_group = flag_group("STATION_NOT_BLOCK")
    parking_not_block_flag_group = flag_group("PARKING_NOT_BLOCK")

    # TODO: connect with storage by #
    # TODO: remove "POINT"
    point_dict = dict_as_dict(
        pp.MatchFirst([
            named_group("type", keyword("RESOURCE_FILLING_POINT")) +
            POINT_VEC3_GROUP,

            named_group("type", keyword("RESOURCE_FILLING_CONVEYOR_POINT")) +
            POINTS_VEC3_2_GROUPS,

            named_group("type", keyword("RESOURCE_INCREASE_POINT")) +
            named_group("storage_index", UNSIGNED_INT) +
            POINT_VEC3_GROUP,

            named_group("type", keyword("RESOURCE_INCREASE_CONVEYOR_POINT")) +
            named_group("storage_index", UNSIGNED_INT) +
            POINTS_VEC3_2_GROUPS,

            named_group("type", pp.MatchFirst([
                keyword("VEHICLE_PARKING_ADVANCED_POINT"),
                keyword("STATION_NOT_BLOCK_DETOUR_POINT"),
                keyword("PARKING_NOT_BLOCK_DETOUR_POINT"),
                keyword("CONNECTION_ADVANCED_POINT"),
            ])) +
            POINT_VEC3_GROUP,

            # TODO: connect with station by #
            named_group("type", pp.MatchFirst([
                keyword("VEHICLE_STATION_NOT_BLOCK_ENTRY_POINT_PID"),
                keyword("STATION_NOT_BLOCK_DETOUR_POINT_PID"),
            ])) +
            named_group("station_index", UNSIGNED_INT) +
            POINT_VEC3_GROUP,

            named_group("type", pp.MatchFirst([
                keyword("VEHICLE_PARKING_ADVANCED_POINT_PID"),
                keyword("PARKING_NOT_BLOCK_DETOUR_POINT_PID"),
                # TODO: connect with parking by #
            ])) +
            named_group("parking_index", UNSIGNED_INT) +
            POINT_VEC3_GROUP,
        ])
    )

    attractive_type_groups = [pp.Opt(pp.Group(keyword(s) + UNSIGNED_INT)) for s in [
        "ATTRACTIVE_TYPE_SWIM",
        "ATTRACTIVE_TYPE_CARUSEL",
        "ATTRACTIVE_TYPE_GALLERY",
        "ATTRACTIVE_TYPE_MUSEUM",
        "ATTRACTIVE_TYPE_SIGHT",
        "ATTRACTIVE_TYPE_ZOO",
    ]]
    attractive_score_groups = [pp.Opt(pp.Group(keyword(s) + UNSIGNED_FLOAT)) for s in [
        "ATTRACTIVE_SCORE",
        "ATTRACTIVE_SCORE_CULTURE",
        "ATTRACTIVE_SCORE_ALCOHOL",
        "ATTRACTIVE_SCORE_SPORT",
        "ATTRACTIVE_SCORE_RELIGION",
    ]]
    attractive_use_forgot_even_match_flag_group = flag_group("ATTRACTIVE_USE_FORGOT_EVEN_MATCH")
    attractive_factor_groups = [pp.Opt(pp.Group(keyword(s) + UNSIGNED_FLOAT)) for s in [
        "ATTRACTIVE_FACTOR_POLLUTION",
        "ATTRACTIVE_FACTOR_POLLUTION_ADD",
        "ATTRACTIVE_FACTOR_SIGHT",
        "ATTRACTIVE_FACTOR_SIGHT_ADD",
        "ATTRACTIVE_FACTOR_NATURE",
        "ATTRACTIVE_FACTOR_NATURE_ADD",
        "ATTRACTIVE_FACTOR_WATER",
        "ATTRACTIVE_FACTOR_WATER_ADD",
    ]]

    heating_group = keyword_name_value_group("HEATING")
    electric_disable_flag_group = flag_group("ELECTRIC_DISABLE")
    watersewage_disable_flag_group = flag_group("WATERSEWAGE_DISABLE")
    sewage_disable_flag_group = flag_group("SEWAGE_DISABLE")

    seasonal_close_if_temp_bellow_group = pp.Group(keyword("SEASONAL_CLOSE_IF_TEMP_BELLOW") + INT)
    seasonal_close_if_temp_above_group = pp.Group(keyword("SEASONAL_CLOSE_IF_TEMP_ABOVE") + INT)

    vehicle_cannotselect_inside_flag_group = flag_group("VEHICLE_CANNOTSELECT_INSIDE")
    worker_rendering_area_dict = keyword("WORKER_RENDERING_AREA").suppress() + dict_as_dict(POINTS_VEC3_2_GROUPS)

    quality_of_living_group = pp.Group(keyword("QUALITY_OF_LIVING") + UNSIGNED_FLOAT)

    engine_speed_group = pp.Group(keyword("ENGINE_SPEED") + FLOAT)

    text_caption_group = pp.Group(keyword("TEXT_CAPTION") + dict_as_dict(POINTS_VEC3_2_GROUPS))

    heliport_area_group = pp.Group(keyword("HELIPORT_AREA") + UNSIGNED_FLOAT)

    working_sfx_group = pp.Group(keyword("WORKING_SFX") + PATH)
    # TODO: Merge
    animation_mesh_group = pp.Group(
        keyword("ANIMATION_MESH") +
        dict_as_dict(
            named_group("nmf", PATH) +
            named_group("mtl", PATH)
        )
    )
    animation_mesh_workshop_group = pp.Group(
        keyword("ANIMATION_MESH_WORKSHOP") +
        dict_as_dict(
            named_group("nmf", PATH) +
            named_group("mtl", PATH)
        )
    )
    underground_mesh_group = pp.Group(
        keyword("UNDERGROUND_MESH") +
        dict_as_dict(
            named_group("nmf", PATH) +
            named_group("mtl", PATH)
        )
    )
    underground_mesh_workshop_group = pp.Group(
        keyword("UNDERGROUND_MESH_WORKSHOP") +
        dict_as_dict(
            named_group("nmf", PATH) +
            named_group("mtl", PATH)
        )
    )
    underground_mesh_depth_offset_group = pp.Group(keyword("UNDERGROUND_MESH_DEPTH_OFFSET") + FLOAT)

    cableway_group = keyword_name_value_group("CABLEWAY")

    water_not_use_for_industry_substations_flag_group = flag_group("WATER_NOT_USE_FOR_INDUSTRY_SUBSTATIONS")

    consumption_water_required_quality_group = pp.Group(keyword("CONSUMPTION_WATER_REQUIRED_QUALITY") + UNSIGNED_FLOAT)
    production_sewage_pollution_group = pp.Group(keyword("PRODUCTION_SEWAGE_POLLUTION") + UNSIGNED_FLOAT)
    outwater_max_quality_group = pp.Group(keyword("OUTWATER_MAX_QUALITY") + UNSIGNED_FLOAT)

    civil_building_flag_group = flag_group("CIVIL_BUILDING")

    harbor_over_water_from_group = pp.Group(keyword("HARBOR_OVER_WATER_FROM") + INT)
    harbor_over_terrain_from_group = pp.Group(keyword("HARBOR_OVER_TERRAIN_FROM") + INT)
    harbor_extend_area_when_bulding_group = pp.Group(keyword("HARBOR_EXTEND_AREA_WHEN_BULDING") + INT)

    water_not_produce_sewage_from_production_flag_group = flag_group("WATER_NOT_PRODUCE_SEWAGE_FROM_PRODUCTION")

    offset_connection_xyzw_dict = dict_as_dict(
        keyword("OFFSET_CONNECTION_XYZW").suppress() +
        # TODO: merge w with xyz
        POINT_VEC3_GROUP +
        named_group("w", FLOAT)
    )
    water_storage_position_group = pp.Group(keyword("WATER_STORAGE_POSITION") + FLOAT)

    # Additive when repeated
    monument_government_loyalty_radius = keyword("MONUMENT_GOVERNMENT_LOYALTY_RADIUS").suppress() + UNSIGNED_INT
    monument_government_loyalty_strength = keyword("MONUMENT_GOVERNMENT_LOYALTY_STRENGTH").suppress() + UNSIGNED_FLOAT

    # TODO: Merge
    production_connect_to_sun_group = pp.Group(keyword("PRODUCTION_CONNECT_TO_SUN") + UNSIGNED_INT)
    production_connect_to_wind_group = pp.Group(keyword("PRODUCTION_CONNECT_TO_WIND") + UNSIGNED_INT)

    consumption_increase_according_year_group = pp.Group(
        keyword("CONSUMPTION_INCREASE_ACCORDING_YEAR") +
        dict_as_dict(
            named_group("year", UNSIGNED_INT) +
            named_group("param2", UNSIGNED_INT) +
            named_group("param3", UNSIGNED_FLOAT)
        )
    )
    production_decrease_according_year_group = pp.Group(
        keyword("PRODUCTION_DECREASE_ACCORDING_YEAR") +
        dict_as_dict(
            named_group("year", UNSIGNED_INT) +
            named_group("param2", UNSIGNED_INT) +
            named_group("param3", UNSIGNED_FLOAT)
        )
    )

    vehicle_passanger_only_flag_group = flag_group("VEHICLE_PASSANGER_ONLY")

    water_storage_fill_full_substation_flag_group = flag_group("WATER_STORAGE_FILL_FULL_SUBSTATION")

    monument_enable_trespassing_flag_group = flag_group("MONUMENT_ENABLE_TRESPASSING")
    monument_eletric_consumption_add_flag_group = flag_group("MONUMENT_ELETRIC_CONSUMPTION_ADD")

    animation_speed_fps_group = pp.Group(keyword("ANIMATION_SPEED_FPS") + UNSIGNED_FLOAT)

    firestation_point_node = keyword("FIRESTATION_POINT_NODE").suppress() + MODEL_PART

    connection_rail_deadend_flag_group = flag_group("CONNECTION_RAIL_DEADEND")

    loddistance1_group = pp.Group(keyword("LODDISTANCE1") + UNSIGNED_INT)
    lod1_group = pp.Group(keyword("LOD1") + PATH)

    disable_mirror_type_flag_group = flag_group("DISABLE_MIRROR_TYPE")
    disable_pedestrian_road_entry_flag_group = flag_group("DISABLE_PEDESTRIAN_ROAD_ENTRY")

    count_limit_group = pp.Group(keyword("COUNT_LIMIT") + UNSIGNED_INT)
    ignore_count_limit_flag_group = flag_group("IGNORE_COUNT_LIMIT")

    border_building_flag_group = flag_group("BORDER_BUILDING")

    # TODO
    # WORKING_SFX_DISTANCE

    building_ini_dict = dict_as_dict(
        each_for_dict(
            unique_values = [
                name_group,
                type_group,
                pp.Opt(subtype_group),
                pp.Opt(workers_needed_group),
                pp.Opt(profesors_needed_group),
                pp.Opt(citizen_able_serve_group),
                pp.Opt(working_vehicles_needed_group),
                pp.Opt(seasonal_close_if_temp_bellow_group),
                pp.Opt(seasonal_close_if_temp_above_group),
                pp.Opt(quality_of_living_group),
                pp.Opt(engine_speed_group),
                pp.Opt(text_caption_group),
                pp.Opt(heating_group),
                pp.Opt(heliport_area_group),
                pp.Opt(working_sfx_group),
                pp.Opt(animation_mesh_group),
                pp.Opt(animation_mesh_workshop_group),
                pp.Opt(underground_mesh_group),
                pp.Opt(underground_mesh_workshop_group),
                pp.Opt(underground_mesh_depth_offset_group),
                pp.Opt(cableway_group),
                pp.Opt(consumption_water_required_quality_group),
                pp.Opt(production_sewage_pollution_group),
                pp.Opt(outwater_max_quality_group),
                pp.Opt(harbor_over_water_from_group),
                pp.Opt(harbor_over_terrain_from_group),
                pp.Opt(harbor_extend_area_when_bulding_group),
                pp.Opt(water_storage_position_group),
                pp.Opt(production_connect_to_sun_group),
                pp.Opt(production_connect_to_wind_group),
                pp.Opt(consumption_increase_according_year_group),
                pp.Opt(production_decrease_according_year_group),
                pp.Opt(animation_speed_fps_group),
                pp.Opt(loddistance1_group),
                pp.Opt(lod1_group),
                pp.Opt(count_limit_group),
                roadvehicle_notflip_flag_group,
                roadvehicle_eletric_flag_group,
                roadvehicle_forklift_pass_flag_group,
                roadvehicle_tram_flag_group,
                long_trains_flag_group,
                station_not_block_flag_group,
                parking_not_block_flag_group,
                attractive_use_forgot_even_match_flag_group,
                vehicle_cannotselect_inside_flag_group,
                electric_disable_flag_group,
                watersewage_disable_flag_group,
                sewage_disable_flag_group,
                water_not_use_for_industry_substations_flag_group,
                civil_building_flag_group,
                water_not_produce_sewage_from_production_flag_group,
                vehicle_passanger_only_flag_group,
                water_storage_fill_full_substation_flag_group,
                monument_enable_trespassing_flag_group,
                monument_eletric_consumption_add_flag_group,
                connection_rail_deadend_flag_group,
                disable_mirror_type_flag_group,
                disable_pedestrian_road_entry_flag_group,
                ignore_count_limit_flag_group,
                border_building_flag_group,
            ] +
            factor_groups +
            resource_source_flag_groups +
            attractive_type_groups +
            attractive_score_groups +
            attractive_factor_groups,
            lists_of_singles = {
                "firestations_points_nodes": firestation_point_node,
                "monument_government_loyalty_radiuses": monument_government_loyalty_radius,
                "monument_government_loyalty_strengths": monument_government_loyalty_strength,
                "pollutions": pollution,
                # "invalid_unknown": UNKNOWN_WORD,
            },
            lists_of_structures = {
                "storages": storage_dict,
                "resources_visualizations": resource_visualization_dict,
                "particles": particle_dict,
                "connections": connection_dict,
                "stations": station_dict,
                "parkings": parking_dict,
                "points": point_dict,
                "workers_rendering_areas": worker_rendering_area_dict,
                "offset_connections_xyzw": offset_connection_xyzw_dict,
            },
            dicts = {
                "productions": production_group,
                "consumptions": consumption_group,
                "consumptions_per_seconds": consumption_per_second_group,
                "costs_works": create_cost_work_group_parser(),
            },
            unknown = True
        ) +
        # TOTEST: Are Opt, caseless, chars after end valid?
        pp.Opt(pp.MatchFirst([pp.Keyword("end", caseless = True), keyword("END")]) + pp.SkipTo(pp.StringEnd())).suppress()
    ).ignore(
        pp.MatchFirst([
            pp.Literal("--"),
            pp.Literal("//"),
            pp.Literal("-$"),
            # TODO: doesn't work
            # pp.LineStart() + pp.Literal("#"),
            pp.Literal("#$"),
            # TODO: doesn't work
            # pp.LineStart() + pp.Literal(";"),
            pp.Literal(";$"),
        ]) +
        pp.restOfLine
    )

    return building_ini_dict
