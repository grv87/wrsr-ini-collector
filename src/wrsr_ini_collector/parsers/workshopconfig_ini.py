from .common import *

def create_workshopconfig_ini_parser():
    item_id_group = pp.Group(keyword("ITEM_ID") + UNSIGNED_INT)
    owner_id_group = pp.Group(keyword("OWNER_ID") + UNSIGNED_INT)
    ITEM_TYPE = UPCASE_TOKEN # TODO
    item_type_group = pp.Group(keyword("ITEM_TYPE") + ITEM_TYPE)
    visibility_group = pp.Group(keyword("VISIBILITY") + UNSIGNED_INT)
    tag = keyword("TAGS").suppress() + UNSIGNED_INT

    object_building = keyword("OBJECT_BUILDING").suppress() + PATH
    object_building_editor_element = keyword("OBJECT_BUILDING_EDITOR_ELEMENT").suppress() + PATH
    target_building_skin_dict = dict_as_dict(
        keyword("TARGET_BUILDING_SKIN").suppress() +
        named_group("path", PATH) +
        named_group("material", PATH) +
        named_group("material_e", PATH)
    )
    object_vehicle = keyword("OBJECT_VEHICLE").suppress() + PATH
    target_object_skin_dict = dict_as_dict(
        keyword("TARGET_OBJECT_SKIN").suppress() +
        named_group("path", PATH) +
        named_group("material", PATH)
    )
    add_custom_sounds2d = keyword("ADD_CUSTOM_SOUNDS2D").suppress() + PATH
    add_custom_sounds3d = keyword("ADD_CUSTOM_SOUNDS3D").suppress() + PATH
    object_script_dict = dict_as_dict(
        keyword("OBJECT_SCRIPT").suppress() +
        named_group("path", PATH) +
        named_group("name", STRING)
    )

    # TODO:
    # $ADD_CUSTOM_TEXT_UTF8

    item_name_group = pp.Group(keyword("ITEM_NAME") + STRING)
    # item_desc_group = pp.Group(keyword("ITEM_DESC") + STRING)
    gui_noborder_flag_group = flag_group("GUI_NOBORDER")
    double_quote = pp.Char('"').suppress()
    end = pp.Opt(double_quote) + gui_noborder_flag_group + keyword("END").suppress()
    item_desc_group = pp.Group(keyword("ITEM_DESC") + double_quote + pp.SkipTo(end))

    workshopconfig_ini_dict = dict_as_dict(
        item_id_group +
        owner_id_group +
        item_type_group +
        visibility_group +
        each_for_dict(
            lists_of_singles = {
                "tags": tag,
                "object_buildings": object_building,
                "object_buildings_editor_elements": object_building_editor_element,
                "object_vehicles": object_vehicle,
                "custom_sounds2d": add_custom_sounds2d,
                "custom_sounds3d": add_custom_sounds3d,
            },
            lists_of_structures = {
                "target_buildings_skins": target_building_skin_dict,
                "target_objects_skins": target_object_skin_dict,
                "objects_scripts": object_script_dict,
            }
        ) +
        item_name_group +
        item_desc_group +
        # gui_noborder_flag_group +
        # keyword("END").suppress()
        end
    )

    return workshopconfig_ini_dict
