from .workshopconfig_ini import create_workshopconfig_ini_parser
from .building_ini import create_building_ini_parser
from .vehicle_script_ini import create_vehicle_script_ini_parser
from pyparsing import enable_all_warnings, ParserElement
from pyparsing.exceptions import ParseException

enable_all_warnings()
workshopconfig_ini_parser = create_workshopconfig_ini_parser()
building_ini_parser = create_building_ini_parser()
vehicle_script_ini_parser = create_vehicle_script_ini_parser()

ParserElement.verbose_stacktrace = True
