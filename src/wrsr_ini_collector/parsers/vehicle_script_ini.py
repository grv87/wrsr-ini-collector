from .common import *

def create_vehicle_script_ini_parser():
    type_group = keyword("TYPE").suppress() + token_name_value_group("VEHICLETYPE")
    # TODO: generalize these
    name_group = str_or_predefined_by_int_group("NAME")
    cost_group = pp.Group(
        pp.MatchFirst([
            keyword("COST_RUB"),
            keyword("COST_USD"),
        ]) +
        UNSIGNED_INT
    )
    movement_speed_group = pp.Group(keyword("MOVEMENT_SPEED") + UNSIGNED_INT)
    movement_power_kw_group = pp.Group(keyword("MOVEMENT_POWER_KW") + UNSIGNED_INT)
    movement_empty_weight_group = pp.Group(keyword("MOVEMENT_EMPTY_WEIGHT") + UNSIGNED_FLOAT)
    movement_hill_speed_group = pp.Group(keyword("MOVEMENT_HILL_SPEED") + UNSIGNED_FLOAT)
    movement_hill_acceleration_group = pp.Group(keyword("MOVEMENT_HILL_ACCELERATION") + UNSIGNED_FLOAT)
    available_group = pp.Group(
        keyword("AVAILABLE") +
        dict_as_dict(
            named_group("start_year", UNSIGNED_INT) +
            named_group("end_year", UNSIGNED_INT)
        )
    )
    resource_transport_group = keyword("RESOURCE_TRANSPORT_TYPE").suppress() + RESOURCE_TRANSPORT_GROUP
    resource_capacity_group = pp.Group(keyword("RESOURCE_CAPACITY") + UNSIGNED_FLOAT)
    resource_allow_only = keyword("RESOURCE_ALLOW_ONLY").suppress() + RESOURCE
    country_group = str_or_predefined_by_int_group("COUNTRY")
    point_dict = dict_as_dict(pp.MatchFirst([
        named_group("type", pp.MatchFirst([
            keyword("LIGHT_BLINKER", False),
            keyword("LIGHT_BLINKER_TRAILER", False),
            keyword("LIGHT_FORWARD", False),
            keyword("LIGHT_BACKWARD", False),
            keyword("MOVEMENT_WHEEL_FRONT", False),
            keyword("MOVEMENT_WHEEL_BACK", False),
            pp.Combine(
                pp.MatchFirst([
                    keyword_start("SIREN"),
                    keyword_start("WATER"),
                    keyword_start("PROPELER"),
                ]) +
                keyword_end("POINT").suppress()
            ),
        ])
        ) +
        POINT_VEC3_GROUP,

        named_group("type", pp.MatchFirst([
            keyword("WAVEPOINTFRONT", False),
            keyword("WAVEPOINTBACK", False),
        ])) +
        pp.Group(pp.Keyword("position") + VEC3_DICT) +
        pp.Group(pp.Keyword("coordstep") + FLOAT) +
        pp.Group(pp.Keyword("bumpfactor") + FLOAT) +
        pp.Group(pp.Keyword("srcsize") + FLOAT) +
        pp.Group(pp.Keyword("dstsize") + FLOAT) +
        pp.Group(pp.Keyword("maxtime") + FLOAT),

        named_group("type", pp.MatchFirst([
            keyword("MIXER_DRUM", False),
            keyword("MIXER_JOINTDRUM", False),
        ])) +
        named_group("model_part", MODEL_PART) +
        POINTS_VEC3_2_GROUPS,

        named_group("type", pp.MatchFirst([
            keyword("DUMPER_DECK_PIVOT", False),
            keyword("DUMPER_JOINTDECK_PIVOT", False),
        ])) +
        named_group("model_part", MODEL_PART) +
        POINT_VEC3_GROUP,
    ]))

    particle_dict = dict_as_dict(pp.MatchFirst([
        named_group("type", pp.Combine(
            keyword_start("PARTICLE").suppress() +
            pp.MatchFirst([
                keyword_end("MOVEMENT_JOINT"),
                keyword_end("MOVEMENT"),
            ])
        )) +
        named_group("effect_name", PARTICLE_EFFECT_NAME) +
        POINT_VEC3_GROUP,

        named_group("type", pp.Combine(
            keyword_start("PARTICLE").suppress() +
            keyword_end("JETPOINT"),
        )) +
        POINT_VEC3_GROUP,
    ]))

    skill_groups = [pp.Opt(pp.Group(keyword(s) + UNSIGNED_INT)) for s in [
        "SKILL_HARVESTING",
        "SKILL_FIRETRUCK",
        "SKILL_WOODCARRING",
        "SKILL_PERSONAL",
        "SKILL_GRAVELMINING",
        "SKILL_CONSTRUCTION_BULLDOZER",
        "SKILL_CONSTRUCTION_GROUNDWORKS",
        "SKILL_CONSTRUCTION_CRANE",
        "SKILL_CONSTRUCTION_ASPHALT_LAYING",
        "SKILL_CONSTRUCTION_ROLLING",
        "SKILL_SEEDING",
        "SKILL_AMBULANCE",
        "SKILL_FORKLIFT",
        "SKILL_FIRELADDER",
        "SKILL_POLICEBUS",
        "SKILL_POLICECAR",
        "SKILL_SNOWPLOW",
        "SKILL_CONSTRUCTION_RAIL_TUNNEL_BORING",
        "HELICOPTER_WATERBUCKET_CAPACITY_TONS",
    ]]
    construction_mixed_skill_flag_group = flag_group("CONSTRUCTION_MIXED_SKILL")

    # TODO: Merge these
    sound_params_path = keyword("SOUND_PARAMS").suppress() + PATH
    sound_params_workshop_path = keyword("SOUND_PARAMS_WORKSHOP").suppress() + PATH

    purchase_exclude_flag_group = flag_group("PURCHASE_EXCLUDE")

    joint_dict = dict_as_dict(
        keyword("JOINT").suppress() +
        named_group("path", PATH) +
        named_group("offset", FLOAT) +
        named_group("rotatingoffset", FLOAT)
    )

    family_group = pp.Group(keyword("FAMILY") + PATH)

    coord_scale_dict = dict_as_dict(
        named_group("coord", VEC3_DICT) +
        named_group("scale", VEC3_DICT)
    )

    resource_visualization_dict = dict_as_dict(
        pp.MatchFirst([
            flag_group_not_opt(pp.Combine(
                keyword_start("RESOURCE_VISUALIZATION").suppress() +
                keyword_end("JOINT")
            )),
            keyword("RESOURCE_VISUALIZATION").suppress(),
        ]) +
        pp.Group(pp.Keyword("rotation") + VEC3_DICT) +
        pp.Group(pp.Keyword("min") + coord_scale_dict) +
        pp.Group(pp.Keyword("max") + coord_scale_dict)
    )
    cargovehicle_visualization_dict = dict_as_dict(
        pp.MatchFirst([
            flag_group_not_opt(pp.Combine(
                keyword_start("CARGOVEHICLE_VISUALIZATION").suppress() +
                keyword_end("JOINT")
            )),
            keyword("CARGOVEHICLE_VISUALIZATION").suppress(),
        ]) +
        pp.Group(pp.Keyword("min") + VEC3_DICT) +
        pp.Group(pp.Keyword("max") + VEC3_DICT)
    )

    cargovehicle_mustbe_loaded_flag_group = flag_group("CARGOVEHICLE_MUSTBE_LOADED")
    cargovehicle_canbe_loaded_flag_group = flag_group("CARGOVEHICLE_CANBE_LOADED")

    working_anim_dict = dict_as_dict(
        keyword("WORKING_ANIM").suppress() +
        named_group("file", PATH) +
        named_group("offsetX", FLOAT) +
        named_group("offsetZ", FLOAT)
    )
    propeler_static_group = pp.Group(keyword("PROPELER_STATIC") + PATH)
    propeler_dynamic_group = pp.Group(keyword("PROPELER_DYNAMIC") + PATH)
    propeler_low_group = pp.Group(
        keyword("PROPELER_LOW") +
        dict_as_dict(
            named_group("unknown", UNSIGNED_FLOAT) +
            named_group("nmf", PATH) +
            named_group("mtl", PATH)
        )
    )
    propeler_helicopter_dict = dict_as_dict(
        keyword("PROPELER_HELICOPTER").suppress() +
        named_group("unknown", UNSIGNED_FLOAT) +
        named_group("nmf_con", PATH) +
        named_group("nmf_hi", PATH) +
        named_group("nmf_lo", PATH) +
        named_group("mtl", PATH)
    )
    propeler_helicopter_point_dir_dict = keyword("PROPELER_HELICOPTER_POINT_DIR").suppress() + dict_as_dict(POINTS_VEC3_2_GROUPS)
    landing_gear_anim_group = pp.Group(keyword("LANDING_GEAR_ANIM") + PATH)

    train_bbox_forward_correction_group = pp.Group(keyword("TRAIN_BBOX_FORWARD_CORRECTION") + FLOAT)
    train_bbox_backward_correction_group = pp.Group(keyword("TRAIN_BBOX_BACKWARD_CORRECTION") + FLOAT)
    train_forward_axis_distance_group = pp.Group(keyword("TRAIN_FORWARD_AXIS_DISTANCE") + FLOAT)
    train_backward_axis_distance_group = pp.Group(keyword("TRAIN_BACKWARD_AXIS_DISTANCE") + FLOAT)

    traingroup_group = keyword_name_value_group("TRAINGROUP")

    trainset_item_dict = dict_as_dict(
        # TODO
        pp.Or([
        # pp.MatchFirst([
            flag_group_not_opt(pp.Combine(
                keyword_start("TRAINSET").suppress() +
                keyword_end("REVERSE")
            )),
            keyword("TRAINSET").suppress(),
        ]) +
        named_group("path", PATH),
    )

    movement_conspumption_group = pp.Group(keyword("MOVEMENT_CONSPUMPTION") + UNSIGNED_INT)
    description_group = str_or_predefined_by_int_group("DESCRIPTION")

    takeoff_distance_group = pp.Group(keyword("TAKEOFF_DISTANCE") + UNSIGNED_INT)

    loddistance1_group = pp.Group(keyword("LODDISTANCE1") + UNSIGNED_INT)
    loddistance2_group = pp.Group(keyword("LODDISTANCE2") + UNSIGNED_INT)
    # TODO: Merge these
    lod1_group = pp.Group(keyword("LOD1") + PATH)
    lod2_group = pp.Group(keyword("LOD2") + PATH)
    lodjoint1_group = pp.Group(keyword("LODJOINT1") + PATH)
    lodjoint2_group = pp.Group(keyword("LODJOINT2") + PATH)

    preview_zoom_group = pp.Group(keyword("PREVIEW_ZOOM") + UNSIGNED_FLOAT)
    preview_offset_group = pp.Group(keyword("PREVIEW_OFFSET") + UNSIGNED_FLOAT)

    alpha_group = pp.Group(keyword("ALPHA") + PATH)

    # TODO: Merge
    skill_construction_rail_track_layer_group = pp.Group(
        keyword("SKILL_CONSTRUCTION_RAIL_TRACK_LAYER") +
        dict_as_dict(
            named_group("replaces_workers", INT) +
            named_group("requires_workers", INT) +
            named_group("unknown3", INT)
        )
    )
    skill_construction_rail_crane_group = pp.Group(
        keyword("SKILL_CONSTRUCTION_RAIL_CRANE") +
        dict_as_dict(
            named_group("replaces_workers", INT) +
            named_group("requires_workers", INT)
        )
    )
    crane_static_group = pp.Group(keyword("CRANE_STATIC") + PATH)
    crane_dynamic_group = pp.Group(
        keyword("CRANE_DYNAMIC") +
        dict_as_dict(
            named_group("path", PATH) +
            POINT_VEC3_GROUP
        )
    )
    crane_dynamic2_group = pp.Group(keyword("CRANE_DYNAMIC2") + PATH)

    tunneling_teeth_flag_group = flag_group("TUNNELING_TEETH")

    stacking_support_group = pp.Group(keyword("STACKING_SUPPORT") + UNSIGNED_INT)
    cabin_group = keyword_name_value_group("CABIN")

    cargo_platform_group = pp.Group(
        keyword("CARGO_PLATFORM") +
        dict_as_dict(
            named_group("path", PATH) +
            pp.Opt(named_group("unknown1", INT)) +
            pp.Opt(named_group("unknown2", INT))
        )
    )

    # TODO:
    # MOVEMENT_OFFROAD
    # BUCKET_SNAP_POINT

    vehicle_script_ini_dict = dict_as_dict(
        each_for_dict(
            unique_values = [
                type_group,
                name_group,
                cost_group,
                # TOTEST: Opt
                pp.Opt(country_group),
                pp.Opt(movement_speed_group),
                pp.Opt(movement_power_kw_group),
                pp.Opt(movement_empty_weight_group),
                pp.Opt(movement_hill_speed_group),
                pp.Opt(movement_hill_acceleration_group),
                pp.Opt(available_group),
                pp.Opt(resource_transport_group),
                pp.Opt(resource_capacity_group),
                pp.Opt(family_group),
                pp.Opt(propeler_static_group),
                pp.Opt(propeler_dynamic_group),
                pp.Opt(propeler_low_group),
                pp.Opt(landing_gear_anim_group),
                pp.Opt(train_bbox_forward_correction_group),
                pp.Opt(train_bbox_backward_correction_group),
                pp.Opt(train_forward_axis_distance_group),
                pp.Opt(train_backward_axis_distance_group),
                pp.Opt(traingroup_group),
                pp.Opt(movement_conspumption_group),
                pp.Opt(description_group),
                pp.Opt(takeoff_distance_group),
                pp.Opt(loddistance1_group),
                pp.Opt(loddistance2_group),
                pp.Opt(lod1_group),
                pp.Opt(lod2_group),
                pp.Opt(lodjoint1_group),
                pp.Opt(lodjoint2_group),
                pp.Opt(preview_zoom_group),
                pp.Opt(preview_offset_group),
                pp.Opt(alpha_group),
                pp.Opt(skill_construction_rail_track_layer_group),
                pp.Opt(skill_construction_rail_crane_group),
                pp.Opt(crane_static_group),
                pp.Opt(crane_dynamic_group),
                pp.Opt(crane_dynamic2_group),
                pp.Opt(stacking_support_group),
                pp.Opt(cabin_group),
                pp.Opt(cargo_platform_group),
                construction_mixed_skill_flag_group,
                purchase_exclude_flag_group,
                cargovehicle_mustbe_loaded_flag_group,
                cargovehicle_canbe_loaded_flag_group,
                tunneling_teeth_flag_group,
            ] +
            skill_groups,
            lists_of_singles= {
                "resources_allow_only": resource_allow_only,
                "sounds_params": sound_params_path,
                "sounds_params_workshops": sound_params_workshop_path,
            },
            lists_of_structures = {
                "points": point_dict,
                "particles": particle_dict,
                "resources_visualizations": resource_visualization_dict,
                "cargovehicles_visualizations": cargovehicle_visualization_dict,
                "joints": joint_dict,
                "working_anims": working_anim_dict,
                "propelers_helicopters": propeler_helicopter_dict,
                "propelers_helicopters_points_dirs": propeler_helicopter_point_dir_dict,
                "trainset": trainset_item_dict,
            },
            unknown = True
        ) +
        # TOTEST: Is it valid?
        pp.Opt(pp.MatchFirst([pp.Keyword("end", caseless = True), keyword("END"), keyword("end")]) + pp.SkipTo(pp.StringEnd())).suppress()
    # TODO: Unify these
    ).ignore(
        pp.MatchFirst([
            pp.Literal("--"),
            pp.Literal("//"),
            pp.Literal("-$"),
            pp.Literal("*$"),
            pp.Literal("#$"),
        ]) +
        pp.restOfLine
    )

    return vehicle_script_ini_dict
